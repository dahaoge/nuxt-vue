module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'nuxt-vue',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: 'Nuxt.js project'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]

  },
  /*
  ** Customize the progress bar color
  */
  loading: {color: '#3B8070'},
  /*
  ** Build configuration
  */
  // modules: ['@nuxtjs/axios'],
  // axios: {
  //   prefix: '/api/', proxy: true // Can be also an object with default options
  // },
  // proxy: {
  //   '/': {
  //     target: 'http://kf.wuyoufang.cn:81/',
  //     pathRewrite: {'^/': ''}
  //   }
  // },

  build: {
    extend(config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        });
      }
    },
    styleResources: {
      scss: ' ./ assets / base.scss ',
      less: ' ./assets /* . less ',
      // sass：...，
      // scss：...
      options: {
        //参见https://github.com/yenshih/style-resources-loader#options
        //除`patterns`属性外
      }
    }
  },
  vender: [
    'element-ui',
    'axios'
  ],
  babel: {
    'plugins': [
      [
        'component', [
        {
          'libraryName': 'element-ui',
          'styleLibraryName': 'theme-default'
        },
        'transform-async-to-generator',
        'transform-runtime'
      ]]],
    comments: true
  },
  plugins: [
    {src: '~plugins/element-ui', ssr: true}
  ],
  css: [
    'element-ui/lib/theme-chalk/index.css'
  ]
};
